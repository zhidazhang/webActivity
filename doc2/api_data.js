define({ "api": [
  {
    "type": "post",
    "url": "/api/check/name",
    "title": "checkName",
    "group": "api_check",
    "version": "1.0.0",
    "description": "<p>检测用户名是否存在</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "userName",
            "description": "<p>用户名</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "false或true //false表示存在,true表示不错在",
          "type": "json"
        },
        {
          "title": "Error",
          "content": "{\ncode:-1\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../controller/api.js",
    "groupTitle": "api_check",
    "name": "PostApiCheckName"
  },
  {
    "type": "post",
    "url": "/api/check/vendor",
    "title": "checkVendor",
    "group": "api_check",
    "version": "1.0.0",
    "description": "<p>检测企业名是否存在</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "name",
            "description": "<p>企业名</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "false或true //false表示存在,true表示不错在",
          "type": "json"
        },
        {
          "title": "Error",
          "content": "{\ncode:-1\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../controller/api.js",
    "groupTitle": "api_check",
    "name": "PostApiCheckVendor"
  },
  {
    "type": "post",
    "url": "/api/login",
    "title": "login",
    "group": "api_login",
    "version": "1.0.0",
    "description": "<p>登录接口</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "username",
            "description": "<p>用户名</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "password",
            "description": "<p>密码</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\ncode:0\n}",
          "type": "json"
        },
        {
          "title": "Error",
          "content": "{\ncode:-1\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../controller/api.js",
    "groupTitle": "api_login",
    "name": "PostApiLogin"
  },
  {
    "type": "post",
    "url": "/api/register/user",
    "title": "registerUser",
    "group": "api_register",
    "version": "1.0.0",
    "description": "<p>注册普通用户</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "userName",
            "description": "<p>用户名</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "password",
            "description": "<p>密码</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "nickname",
            "description": "<p>昵称</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\ncode:0\n}",
          "type": "json"
        },
        {
          "title": "Error",
          "content": "{\ncode:-1\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../controller/api.js",
    "groupTitle": "api_register",
    "name": "PostApiRegisterUser"
  },
  {
    "type": "post",
    "url": "/api/register/vendor",
    "title": "registerVendor",
    "group": "api_register",
    "version": "1.0.0",
    "description": "<p>注册企业用户(限AR业务)</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "userName",
            "description": "<p>用户名</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "password",
            "description": "<p>密码</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "name",
            "description": "<p>企业名称</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "phone",
            "description": "<p>联系座机</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "mobile",
            "description": "<p>联系手机</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "remark",
            "description": "<p>企业描述</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\ncode:0\n}",
          "type": "json"
        },
        {
          "title": "Error",
          "content": "{\ncode:-1\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../controller/api.js",
    "groupTitle": "api_register",
    "name": "PostApiRegisterVendor"
  },
  {
    "type": "post",
    "url": "/api/service/admin/data",
    "title": "adminData",
    "group": "api_service",
    "version": "1.0.0",
    "description": "<p>根据sd获取服务的详细信息</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "sd",
            "description": "<p>serviceData的id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "success",
          "content": "{\ncode:0,\ndata:{\n   id:1,\n   name:'xx',\n   description:'xx',\n   content:'',\n   image:'',\n   msgAttrs:{},  //扩展字段\n   vid:1,        //发布公司id\n   uid:1,        //发布用户id\n   ts:1111       //发布时间戳\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../controller/service.js",
    "groupTitle": "api_service",
    "name": "PostApiServiceAdminData"
  },
  {
    "type": "post",
    "url": "/api/service/admin/delete",
    "title": "adminDelete",
    "group": "api_service",
    "version": "1.0.0",
    "description": "<p>根据serviceData id删除数据</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "optional": false,
            "field": "sd",
            "description": "<p>{int} serviceData id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "success",
          "content": "{\ncode:0\n}",
          "type": "json"
        },
        {
          "title": "error",
          "content": "{\ncode:-1\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../controller/service.js",
    "groupTitle": "api_service",
    "name": "PostApiServiceAdminDelete"
  },
  {
    "type": "post",
    "url": "/api/service/admin/list/data",
    "title": "adminDataList",
    "group": "api_service",
    "version": "1.0.0",
    "description": "<p>根据vid,st获取vid发布的服务</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "vid",
            "description": "<p>企业id</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": true,
            "field": "st",
            "description": "<p>serviceType的Id,不指定代表所有类型.</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "page",
            "description": "<p>数据页码(从0开始)</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "limit",
            "description": "<p>数据最多条数</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "success",
          "content": "{\n code:0,\n list:[{\n     id:1,\n     name:'',\n     description:'',\n     content:'',\n     image:'',\n     msgAttrs:'',\n     ts:''\n     },...]\n}",
          "type": "json"
        },
        {
          "title": "error",
          "content": "{\ncode:-1\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../controller/service.js",
    "groupTitle": "api_service",
    "name": "PostApiServiceAdminListData"
  },
  {
    "type": "post",
    "url": "/api/service/admin/list/vendor",
    "title": "adminVendorList",
    "group": "api_service",
    "version": "1.0.0",
    "description": "<p>获取本用户有操作权限的企业列表</p>",
    "success": {
      "examples": [
        {
          "title": "success",
          "content": "{\n code:0,\n list:[{\n        id:1,\n        name:'xx'\n      },...]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../controller/service.js",
    "groupTitle": "api_service",
    "name": "PostApiServiceAdminListVendor"
  },
  {
    "type": "post",
    "url": "/api/service/admin/update",
    "title": "adminUpdate",
    "group": "api_service",
    "version": "1.0.0",
    "description": "<p>更新指定sd的数据</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "sd",
            "description": "<p>服务数据的id</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "name",
            "description": "<p>服务名</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "description",
            "description": "<p>描述</p>"
          },
          {
            "group": "Parameter",
            "type": "text",
            "optional": false,
            "field": "content",
            "description": "<p>内容</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "image",
            "description": "<p>缩略图</p>"
          },
          {
            "group": "Parameter",
            "type": "object",
            "optional": false,
            "field": "msgAttrs",
            "description": "<p>额外字段(使用jquery该参数使用json.stringfy()转换)</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "success",
          "content": "{\n code:0\n}",
          "type": "json"
        },
        {
          "title": "error",
          "content": "{\ncode:-1\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../controller/service.js",
    "groupTitle": "api_service",
    "name": "PostApiServiceAdminUpdate"
  },
  {
    "type": "post",
    "url": "/api/service/publish/submit",
    "title": "publishSubmit",
    "group": "api_service",
    "version": "1.0.0",
    "description": "<p>提交添加新服务信息</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "st",
            "description": "<p>服务类型id</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "vid",
            "description": "<p>企业id</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "name",
            "description": "<p>服务名</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "description",
            "description": "<p>描述</p>"
          },
          {
            "group": "Parameter",
            "type": "text",
            "optional": false,
            "field": "content",
            "description": "<p>内容</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "image",
            "description": "<p>缩略图</p>"
          },
          {
            "group": "Parameter",
            "type": "object",
            "optional": false,
            "field": "msgAttrs",
            "description": "<p>额外字段(使用jquery该参数使用json.stringfy()转换)</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "action",
            "description": "<p>服务对应action</p>"
          },
          {
            "group": "Parameter",
            "type": "array",
            "optional": false,
            "field": "target",
            "description": "<p>发布目标企业id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "success",
          "content": "{\n code:0\n}",
          "type": "json"
        },
        {
          "title": "error",
          "content": "{\n code:-1\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../controller/service.js",
    "groupTitle": "api_service",
    "name": "PostApiServicePublishSubmit"
  },
  {
    "type": "post",
    "url": "/api/service/user/data",
    "title": "userData",
    "group": "api_service",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "sd",
            "description": "<p>数据id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "success",
          "content": "{\ncode:0,\ndata:{\n   id:1,\n   name:'xx',\n   description:'xx',\n   content:'',\n   image:'',\n   msgAttrs:{},  //扩展字段\n   vid:1,        //发布公司id\n   uid:1,        //发布用户id\n   ts:1111       //发布时间戳\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../controller/service.js",
    "groupTitle": "api_service",
    "name": "PostApiServiceUserData"
  },
  {
    "type": "post",
    "url": "/api/service/user/list/data",
    "title": "userListData",
    "group": "api_service",
    "version": "1.0.0",
    "description": "<p>根据st获取当前用户可见的所有serviceData</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": true,
            "field": "st",
            "description": "<p>serviceType的Id,不指定代表所有类型.</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "page",
            "description": "<p>数据页码(从0开始)</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "limit",
            "description": "<p>数据最多条数</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "success",
          "content": "{\n code:0,\n list:[{\n     id:1,\n     name:'',\n     description:'',\n     content:'',\n     image:'',\n     msgAttrs:'',\n     ts:''\n     },...]\n}",
          "type": "json"
        },
        {
          "title": "error",
          "content": "{\ncode:-1\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../controller/service.js",
    "groupTitle": "api_service",
    "name": "PostApiServiceUserListData"
  },
  {
    "type": "post",
    "url": "/api/service/user/submit",
    "title": "userSubmit",
    "group": "api_service",
    "version": "1.0.0",
    "description": "<p>修改用户对应该sd服务的个人数据</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "sd",
            "description": "<p>serviceData Id</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "status",
            "description": "<p>状态符</p>"
          },
          {
            "group": "Parameter",
            "type": "object",
            "optional": false,
            "field": "msgAttrs",
            "description": "<p>额外字段 (使用jquery该参数使用json.stringfy()转换)</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "success",
          "content": "{\ncode:0\n}",
          "type": "json"
        },
        {
          "title": "error",
          "content": "{\ncode:-1\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../controller/service.js",
    "groupTitle": "api_service",
    "name": "PostApiServiceUserSubmit"
  },
  {
    "type": "get",
    "url": "/api/upload/file/:fid",
    "title": "getFile",
    "group": "api_upload",
    "version": "1.0.0",
    "description": "<p>根据fid获取文件</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "fid",
            "description": "<p>文件fid</p>"
          }
        ]
      }
    },
    "filename": "../controller/fileUpload.js",
    "groupTitle": "api_upload",
    "name": "GetApiUploadFileFid"
  },
  {
    "type": "get",
    "url": "/api/upload/images/:fid",
    "title": "getImage",
    "group": "api_upload",
    "version": "1.0.0",
    "description": "<p>根据fid获取图片</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "fid",
            "description": "<p>文件fid</p>"
          }
        ]
      }
    },
    "filename": "../controller/fileUpload.js",
    "groupTitle": "api_upload",
    "name": "GetApiUploadImagesFid"
  },
  {
    "type": "get",
    "url": "/api/upload/status",
    "title": "uploadStatus",
    "group": "api_upload",
    "version": "1.0.0",
    "description": "<p>上传进度获取</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "file",
            "description": "<p>文件第一块分片md5</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "success",
          "content": "{\n code:0\n size:size   //在服务器上已经上传文件大小\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../controller/fileUpload.js",
    "groupTitle": "api_upload",
    "name": "GetApiUploadStatus"
  },
  {
    "type": "get",
    "url": "/api/upload/thumbnail/:fid",
    "title": "getThumbnail",
    "group": "api_upload",
    "version": "1.0.0",
    "description": "<p>根据fid获取图片缩略图</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "fid",
            "description": "<p>文件fid</p>"
          }
        ]
      }
    },
    "filename": "../controller/fileUpload.js",
    "groupTitle": "api_upload",
    "name": "GetApiUploadThumbnailFid"
  },
  {
    "type": "post",
    "url": "/api/upload",
    "title": "upload",
    "group": "api_upload",
    "version": "1.0.0",
    "description": "<p>分片上传文件接口</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "type",
            "description": "<p>文件类型,例:&quot;image/jpeg&quot;</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "_chunkNumber",
            "description": "<p>上传的分块的序列</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "_chunkSize",
            "description": "<p>上传的分块的大小固定为2048000</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "_currentChunkSize",
            "description": "<p>当前上传的块的大小</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "_totalSize",
            "description": "<p>文件的总大小</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "md5",
            "description": "<p>文件第一块分块的md5 可以使用sprk-md5生成,分块大于文件大小,则为整个文件的md5</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "success",
          "content": "{\n code:0,\n fid:'fid'\n}",
          "type": "json"
        },
        {
          "title": "error",
          "content": "{\n code:-1\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../controller/fileUpload.js",
    "groupTitle": "api_upload",
    "name": "PostApiUpload"
  },
  {
    "type": "post",
    "url": "/api/upload/cancel",
    "title": "cancelUpload",
    "group": "api_upload",
    "version": "1.0.0",
    "description": "<p>根据fid取消上传的文件</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "fid",
            "description": "<p>文件id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "success",
          "content": "{\n code:0\n}",
          "type": "json"
        },
        {
          "title": "error",
          "content": "{\n code:-1或1\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../controller/fileUpload.js",
    "groupTitle": "api_upload",
    "name": "PostApiUploadCancel"
  },
  {
    "type": "post",
    "url": "/api/user/info",
    "title": "userInfo",
    "group": "api_user",
    "version": "1.0.0",
    "description": "<p>获取用户本人的详细信息</p>",
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": " {\n code:0\n user:{\n     nickname:'昵称',\n     sex:0               //0--男,1--女\n     weixin:123          //微信\n     qq:123              //qq\n     email:123@XXX.COM\n     phone:123456\n     photoId:'fid'       //头像文件fid\n     extattrs:{}         //扩展字段\n     }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../controller/api.js",
    "groupTitle": "api_user",
    "name": "PostApiUserInfo"
  },
  {
    "type": "post",
    "url": "/api/user/password",
    "title": "userPassword",
    "group": "api_user",
    "version": "1.0.0",
    "description": "<p>更改密码</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "name",
            "description": "<p>用户名</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "password",
            "description": "<p>旧密码</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "newPassword",
            "description": "<p>新密码</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\ncode:0\n}",
          "type": "json"
        },
        {
          "title": "error",
          "content": "{\ncode:-1\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../controller/api.js",
    "groupTitle": "api_user",
    "name": "PostApiUserPassword"
  },
  {
    "type": "post",
    "url": "/api/user/update",
    "title": "updateUser",
    "group": "api_user",
    "version": "1.0.0",
    "description": "<p>更新用户本人的个人信息</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "nickname",
            "description": "<p>昵称</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": true,
            "field": "sex",
            "description": "<p>性别</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "weixin",
            "description": "<p>微信号</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "qq",
            "description": "<p>qq</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "phone",
            "description": "<p>电话</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": true,
            "field": "photoId",
            "description": "<p>头像文件fid</p>"
          },
          {
            "group": "Parameter",
            "type": "object",
            "optional": true,
            "field": "extattrs",
            "description": "<p>扩展字段(使用jquery该参数使用json.stringfy()转换)</p>"
          }
        ]
      }
    },
    "filename": "../controller/api.js",
    "groupTitle": "api_user",
    "name": "PostApiUserUpdate"
  },
  {
    "type": "get",
    "url": "/images/:fid",
    "title": "getImage",
    "group": "fileUpload",
    "version": "1.0.0",
    "description": "<p>根据fid获取图片</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "fid",
            "description": "<p>文件fid</p>"
          }
        ]
      }
    },
    "filename": "../controller/fileUpload.js",
    "groupTitle": "fileUpload",
    "name": "GetImagesFid"
  },
  {
    "type": "get",
    "url": "/thumbnail/:fid",
    "title": "getThumbnail",
    "group": "fileUpload",
    "version": "1.0.0",
    "description": "<p>根据fid获取图片缩略图</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "fid",
            "description": "<p>文件fid</p>"
          }
        ]
      }
    },
    "filename": "../controller/fileUpload.js",
    "groupTitle": "fileUpload",
    "name": "GetThumbnailFid"
  },
  {
    "type": "get",
    "url": "/upload/file/:fid",
    "title": "getFile",
    "group": "fileUpload",
    "version": "1.0.0",
    "description": "<p>根据fid获取文件</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "fid",
            "description": "<p>文件fid</p>"
          }
        ]
      }
    },
    "filename": "../controller/fileUpload.js",
    "groupTitle": "fileUpload",
    "name": "GetUploadFileFid"
  },
  {
    "type": "get",
    "url": "/upload/status",
    "title": "uploadStatus",
    "group": "fileUpload",
    "version": "1.0.0",
    "description": "<p>上传进度获取</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "file",
            "description": "<p>文件第一块分片md5</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "success",
          "content": "{\n code:0\n size:size   //在服务器上已经上传文件大小\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../controller/fileUpload.js",
    "groupTitle": "fileUpload",
    "name": "GetUploadStatus"
  },
  {
    "type": "post",
    "url": "/upload",
    "title": "upload",
    "group": "fileUpload",
    "version": "1.0.0",
    "description": "<p>分片上传文件接口</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "type",
            "description": "<p>文件类型,例:&quot;image/jpeg&quot;</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "_chunkNumber",
            "description": "<p>上传的分块的序列</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "_chunkSize",
            "description": "<p>上传的分块的大小固定为2048000</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "_currentChunkSize",
            "description": "<p>当前上传的块的大小</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "_totalSize",
            "description": "<p>文件的总大小</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "md5",
            "description": "<p>文件第一块分块的md5 可以使用sprk-md5生成,分块大于文件大小,则为整个文件的md5</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "success",
          "content": "{\n code:0,\n fid:'fid'\n}",
          "type": "json"
        },
        {
          "title": "error",
          "content": "{\n code:-1\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../controller/fileUpload.js",
    "groupTitle": "fileUpload",
    "name": "PostUpload"
  },
  {
    "type": "post",
    "url": "/upload/cancel",
    "title": "cancelUpload",
    "group": "fileUpload",
    "version": "1.0.0",
    "description": "<p>根据fid取消上传的文件</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "fid",
            "description": "<p>文件id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "success",
          "content": "{\n code:0\n}",
          "type": "json"
        },
        {
          "title": "error",
          "content": "{\n code:-1或1\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../controller/fileUpload.js",
    "groupTitle": "fileUpload",
    "name": "PostUploadCancel"
  },
  {
    "type": "get",
    "url": "/service/admin/action/:action",
    "title": "adminAction",
    "group": "service_admin",
    "version": "1.0.0",
    "description": "<p>获取action相应id的admin端html</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": ":action",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": true,
            "field": "id",
            "description": "<p>可以给一个action对应多个html</p>"
          }
        ]
      }
    },
    "filename": "../controller/service.js",
    "groupTitle": "service_admin",
    "name": "GetServiceAdminActionAction"
  },
  {
    "type": "post",
    "url": "/service/admin/data",
    "title": "adminData",
    "group": "service_admin",
    "version": "1.0.0",
    "description": "<p>根据sd获取服务的详细信息</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "sd",
            "description": "<p>serviceData的id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "success",
          "content": "{\ncode:0,\ndata:{\n   id:1,\n   name:'xx',\n   description:'xx',\n   content:'',\n   image:'',\n   msgAttrs:{},  //扩展字段\n   vid:1,        //发布公司id\n   uid:1,        //发布用户id\n   ts:1111       //发布时间戳\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../controller/service.js",
    "groupTitle": "service_admin",
    "name": "PostServiceAdminData"
  },
  {
    "type": "post",
    "url": "/service/admin/delete",
    "title": "adminDelete",
    "group": "service_admin",
    "version": "1.0.0",
    "description": "<p>根据serviceData id删除数据</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "optional": false,
            "field": "sd",
            "description": "<p>{int} serviceData id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "success",
          "content": "{\ncode:0\n}",
          "type": "json"
        },
        {
          "title": "error",
          "content": "{\ncode:-1\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../controller/service.js",
    "groupTitle": "service_admin",
    "name": "PostServiceAdminDelete"
  },
  {
    "type": "post",
    "url": "/service/admin/list/data",
    "title": "adminDataList",
    "group": "service_admin",
    "version": "1.0.0",
    "description": "<p>根据vid,st获取vid发布的服务</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "vid",
            "description": "<p>企业id</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": true,
            "field": "st",
            "description": "<p>serviceType的Id,不指定代表所有类型.</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "page",
            "description": "<p>数据页码(从0开始)</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "limit",
            "description": "<p>数据最多条数</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "success",
          "content": "{\n code:0,\n list:[{\n     id:1,\n     name:'',\n     description:'',\n     content:'',\n     image:'',\n     msgAttrs:'',\n     ts:''\n     },...]\n}",
          "type": "json"
        },
        {
          "title": "error",
          "content": "{\ncode:-1\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../controller/service.js",
    "groupTitle": "service_admin",
    "name": "PostServiceAdminListData"
  },
  {
    "type": "post",
    "url": "/service/admin/list/type",
    "title": "adminTypeList",
    "group": "service_admin",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "vid",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\ncode:0,\nlist:[{\n      id:,\n      name:,\n      remark:, //服务描述\n      },...]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../controller/service.js",
    "groupTitle": "service_admin",
    "name": "PostServiceAdminListType"
  },
  {
    "type": "post",
    "url": "/service/admin/list/vendor",
    "title": "adminVendorList",
    "group": "service_admin",
    "version": "1.0.0",
    "description": "<p>获取本用户有操作权限的企业列表</p>",
    "success": {
      "examples": [
        {
          "title": "success",
          "content": "{\n code:0,\n list:[{\n        id:1,\n        name:'xx'\n      },...]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../controller/service.js",
    "groupTitle": "service_admin",
    "name": "PostServiceAdminListVendor"
  },
  {
    "type": "post",
    "url": "/service/admin/list/vendor",
    "title": "adminVendorList",
    "group": "service_admin",
    "version": "1.0.0",
    "description": "<p>获取本用户有操作权限的企业列表</p>",
    "success": {
      "examples": [
        {
          "title": "success",
          "content": "{\n code:0,\n list:[{\n        id:1,\n        name:'xx'\n      },...]\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../controller/service.js",
    "groupTitle": "service_admin",
    "name": "PostServiceAdminListVendor"
  },
  {
    "type": "post",
    "url": "/service/admin/loader",
    "title": "adminLoader",
    "group": "service_admin",
    "version": "1.0.0",
    "description": "<p>根据sd获取action</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "sd",
            "description": "<p>serviceData id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "success",
          "content": "{\n code:0,\n action:'register'\n}",
          "type": "json"
        },
        {
          "title": "error",
          "content": "{\n code:-1\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../controller/service.js",
    "groupTitle": "service_admin",
    "name": "PostServiceAdminLoader"
  },
  {
    "type": "post",
    "url": "/service/admin/update",
    "title": "adminUpdate",
    "group": "service_admin",
    "version": "1.0.0",
    "description": "<p>更新指定sd的数据</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "sd",
            "description": "<p>服务数据的id</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "name",
            "description": "<p>服务名</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "description",
            "description": "<p>描述</p>"
          },
          {
            "group": "Parameter",
            "type": "text",
            "optional": false,
            "field": "content",
            "description": "<p>内容</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "image",
            "description": "<p>缩略图</p>"
          },
          {
            "group": "Parameter",
            "type": "object",
            "optional": false,
            "field": "msgAttrs",
            "description": "<p>额外字段</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "success",
          "content": "{\n code:0\n}",
          "type": "json"
        },
        {
          "title": "error",
          "content": "{\ncode:-1\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../controller/service.js",
    "groupTitle": "service_admin",
    "name": "PostServiceAdminUpdate"
  },
  {
    "type": "get",
    "url": "/service/publish/action/:action",
    "title": "publishAction",
    "group": "service_publish",
    "version": "1.0.0",
    "description": "<p>获取action相应id的publish端html</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": ":action",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": true,
            "field": "id",
            "description": "<p>可以给一个action对应多个html</p>"
          }
        ]
      }
    },
    "filename": "../controller/service.js",
    "groupTitle": "service_publish",
    "name": "GetServicePublishActionAction"
  },
  {
    "type": "post",
    "url": "/service/publish/loader",
    "title": "publishLoader",
    "group": "service_publish",
    "version": "1.0.0",
    "description": "<p>根据st获取action</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "st",
            "description": "<p>服务id</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "vid",
            "description": "<p>企业id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "success",
          "content": "{\n code:0,\n action:'register'\n}",
          "type": "json"
        },
        {
          "title": "error",
          "content": "{\n code:-1\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../controller/service.js",
    "groupTitle": "service_publish",
    "name": "PostServicePublishLoader"
  },
  {
    "type": "post",
    "url": "/service/publish/submit",
    "title": "publishSubmit",
    "group": "service_publish",
    "version": "1.0.0",
    "description": "<p>提交添加新服务信息</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "st",
            "description": "<p>服务类型id</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "vid",
            "description": "<p>企业id</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "name",
            "description": "<p>服务名</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "description",
            "description": "<p>描述</p>"
          },
          {
            "group": "Parameter",
            "type": "text",
            "optional": false,
            "field": "content",
            "description": "<p>内容</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "image",
            "description": "<p>缩略图</p>"
          },
          {
            "group": "Parameter",
            "type": "object",
            "optional": false,
            "field": "msgAttrs",
            "description": "<p>额外字段</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "action",
            "description": "<p>服务对应action</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "target",
            "description": "<p>发布目标企业id,如1,2,3 用逗号隔开</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "success",
          "content": "{\n code:0\n}",
          "type": "json"
        },
        {
          "title": "error",
          "content": "{\n code:-1\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../controller/service.js",
    "groupTitle": "service_publish",
    "name": "PostServicePublishSubmit"
  },
  {
    "type": "post",
    "url": "/service/publish/target",
    "title": "publishTarget",
    "group": "service_publish",
    "version": "1.0.0",
    "description": "<p>获取可发布目标</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "vid",
            "description": "<p>企业id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "success",
          "content": "{\n  code:0,\n  target[\n     {id:1,\n      name:'xx',\n      parentId:0\n      },...\n  ]\n}",
          "type": "json"
        },
        {
          "title": "error",
          "content": "{\ncode:-1\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../controller/service.js",
    "groupTitle": "service_publish",
    "name": "PostServicePublishTarget"
  },
  {
    "type": "get",
    "url": "/service/user/action/:action",
    "title": "userAction",
    "group": "service_user",
    "version": "1.0.0",
    "description": "<p>获取action相应id的user端html</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": ":action",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": true,
            "field": "id",
            "description": "<p>可以给一个action对应多个html</p>"
          }
        ]
      }
    },
    "filename": "../controller/service.js",
    "groupTitle": "service_user",
    "name": "GetServiceUserActionAction"
  },
  {
    "type": "post",
    "url": "/service/user/action/:action",
    "title": "",
    "group": "service_user",
    "version": "1.0.0",
    "description": "<p>具体action的特殊操作</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": ":action",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "object",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      }
    },
    "filename": "../controller/service.js",
    "groupTitle": "service_user",
    "name": "PostServiceUserActionAction"
  },
  {
    "type": "post",
    "url": "/service/user/data",
    "title": "userData",
    "group": "service_user",
    "version": "1.0.0",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "sd",
            "description": "<p>数据id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "success",
          "content": "{\ncode:0,\ndata:{\n   id:1,\n   name:'xx',\n   description:'xx',\n   content:'',\n   image:'',\n   msgAttrs:{},  //扩展字段\n   vid:1,        //发布公司id\n   uid:1,        //发布用户id\n   ts:1111       //发布时间戳\n  }\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../controller/service.js",
    "groupTitle": "service_user",
    "name": "PostServiceUserData"
  },
  {
    "type": "post",
    "url": "/service/user/list/data",
    "title": "userListData",
    "group": "service_user",
    "version": "1.0.0",
    "description": "<p>根据st获取当前用户可见的所有serviceData</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": true,
            "field": "st",
            "description": "<p>serviceType的Id,不指定代表所有类型.</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "page",
            "description": "<p>数据页码(从0开始)</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "limit",
            "description": "<p>数据最多条数</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "success",
          "content": "{\n code:0,\n list:[{\n     id:1,\n     name:'',\n     description:'',\n     content:'',\n     image:'',\n     msgAttrs:'',\n     ts:''\n     },...]\n}",
          "type": "json"
        },
        {
          "title": "error",
          "content": "{\ncode:-1\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../controller/service.js",
    "groupTitle": "service_user",
    "name": "PostServiceUserListData"
  },
  {
    "type": "post",
    "url": "/service/user/list/personal",
    "title": "userPersonalList",
    "group": "service_user",
    "version": "1.0.0",
    "description": "<p>获取个人数据中status为指定值的服务列表</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "optional": false,
            "field": "status",
            "description": ""
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "st",
            "description": ""
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "limit",
            "description": ""
          },
          {
            "group": "Parameter",
            "optional": false,
            "field": "page",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "success",
          "content": "{\n code:0,\n list:[{\n     id:1,\n     name:'',\n     description:'',\n     image:'',\n     msgAttrs:'',\n     ts:''\n     },...]\n}",
          "type": "json"
        },
        {
          "title": "error",
          "content": "{\ncode:-1\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../controller/service.js",
    "groupTitle": "service_user",
    "name": "PostServiceUserListPersonal"
  },
  {
    "type": "post",
    "url": "/service/user/loader",
    "title": "userLoader",
    "group": "service_user",
    "version": "1.0.0",
    "description": "<p>根据serviceData id获取action</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "optional": false,
            "field": "sd",
            "description": "<p>{int} serviceData id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "success",
          "content": "{\n code:0,\n action:'register'\n}",
          "type": "json"
        },
        {
          "title": "error",
          "content": "{\ncode:-1\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../controller/service.js",
    "groupTitle": "service_user",
    "name": "PostServiceUserLoader"
  },
  {
    "type": "post",
    "url": "/service/user/personal",
    "title": "userPersonal",
    "group": "service_user",
    "version": "1.0.0",
    "description": "<p>根据sd获取对于该服务的个人数据</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "sd",
            "description": "<p>serviceData的id</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "Success",
          "content": "{\ncode:0,\ndata:[{              //数据最多为一条,没有说明没有添加\n       id:1,\n       sd:1,\n       uid:1,\n       errcode:0,\n       status:0,\n       msgAttrs:{},\n       ts:1234\n      }]\n}",
          "type": "json"
        },
        {
          "title": "Error",
          "content": "{\ncode:-1\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../controller/service.js",
    "groupTitle": "service_user",
    "name": "PostServiceUserPersonal"
  },
  {
    "type": "post",
    "url": "/service/user/submit",
    "title": "userSubmit",
    "group": "service_user",
    "version": "1.0.0",
    "description": "<p>修改用户对应该sd服务的个人数据</p>",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "sd",
            "description": "<p>serviceData Id</p>"
          },
          {
            "group": "Parameter",
            "type": "int",
            "optional": false,
            "field": "status",
            "description": "<p>状态符</p>"
          },
          {
            "group": "Parameter",
            "type": "object",
            "optional": false,
            "field": "msgAttrs",
            "description": "<p>额外字段</p>"
          }
        ]
      }
    },
    "success": {
      "examples": [
        {
          "title": "success",
          "content": "{\ncode:0\n}",
          "type": "json"
        },
        {
          "title": "error",
          "content": "{\ncode:-1\n}",
          "type": "json"
        }
      ]
    },
    "filename": "../controller/service.js",
    "groupTitle": "service_user",
    "name": "PostServiceUserSubmit"
  }
] });
